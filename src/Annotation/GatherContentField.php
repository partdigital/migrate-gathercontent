<?php

namespace Drupal\migrate_gathercontent\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a GatherContentField annotation object.
 *
 * @Annotation
 *
 * @see \Drupal\migrate_gathercontent\Plugin\MigrateFieldPluginManager
 * @see \Drupal\migrate_gathercontent\Plugin\migrate\gathercontent_field\FieldPluginBase
 *
 * @ingroup migration
 *
 */
class GatherContentField extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the field type.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * An array of field types the widget supports.
   *
   * @var array
   */
  public $field_types = [];

}
