<?php

namespace Drupal\migrate_gathercontent;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a group entity.
 */
interface GroupInterface extends ConfigEntityInterface {

}
