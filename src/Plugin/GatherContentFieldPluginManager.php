<?php

namespace Drupal\migrate_gathercontent\Plugin;

use Drupal\migrate\Plugin\MigratePluginManager;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;

/**
 * Plugin type manager for field widgets.
 *
 * @ingroup migration
 */
class GatherContentFieldPluginManager extends MigratePluginManager {

  /**
   * {@inheritdoc}
   */
  public function getPluginIdFromFieldType($field_type, array $configuration = [], MigrationInterface $migration = NULL) {
    // TODO: It's not loading the definitions.
    $definitions = $this->getDefinitions();
    foreach ($definitions as $plugin_id => $definition) {
      if (in_array($field_type, $definition['field_types']) || $field_type === $plugin_id) {
        return $plugin_id;
      }
    }
    throw new PluginNotFoundException($field_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginIdFromFieldName($field_name, array $configuration = [], MigrationInterface $migration = NULL) {
    // TODO: This is still work in progress.
    $definitions = $this->getDefinitions();
    foreach ($definitions as $plugin_id => $definition) {
      if (in_array($field_name, $definition['field_types']) || $field_name === $plugin_id) {
        return $plugin_id;
      }
    }
    throw new PluginNotFoundException($field_name);
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);

   /* foreach (['core', 'source_module', 'destination_module'] as $required_property) {
      if (empty($definition[$required_property])) {
        throw new BadPluginDefinitionException($plugin_id, $required_property);
      }
    }*/
  }
}
