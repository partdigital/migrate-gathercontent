<?php

namespace Drupal\migrate_gathercontent\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;

/**
 * Provides an interface for all field type plugins.
 */
interface GatherContentFieldInterface extends PluginInspectionInterface {


  /**
   * Apply any custom processing to the field bundle migrations.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration entity.
   * @param string $field_name
   *   The field name we're processing the value for.
   * @param array $data
   *   The array of field data from FieldValues::fieldData().
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $data);

  /**
   * Computes the destination type of a migrated field.
   *
   * @param \Drupal\migrate\Row $row
   *   The field being migrated.
   *
   * @return string
   *   The destination field type.
   */
  public function getFieldType(Row $row);

}
