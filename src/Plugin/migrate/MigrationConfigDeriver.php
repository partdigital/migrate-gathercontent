<?php

namespace Drupal\migrate_gathercontent\Plugin\migrate;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\migrate\Plugin\MigrationPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\migrate\Plugin\MigrationDeriverTrait;
use Drupal\migrate_gathercontent\DrupalGatherContentClient;
use Drupal\migrate_gathercontent\Plugin\GatherContentFieldPluginManager;
use Drupal\Core\Entity\EntityFieldManagerInterface;

/**
 * Deriver GatherContent configurations.
 */
class MigrationConfigDeriver extends DeriverBase implements ContainerDeriverInterface {
  use MigrationDeriverTrait;

  /**
   * The base plugin ID this derivative is for.
   *
   * @var string
   */
  protected $basePluginId;

  /**
   * The GatherContent client
   *
   * @var \Drupal\migrate_gathercontent\DrupalGatherContentClient
   */
  protected $client;

  /**
   * Entity Type Manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\migrate\Plugin\MigrationPluginManager $migrationPluginManager
   *
   * The migration plugin manager.
   */
  protected $migrationPluginManager;

  /**
   * The gathercontent field plugin manager.
   *
   * @var \Drupal\migrate_gathercontent\Plugin\GatherContentFieldPluginManager $gatherContentFieldPluginManager
   */
  protected $gatherContentFieldPluginManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * List of destination exceptions.
   *
   * // TODO: This should be managed with a plugin.
   * @var array
   */
  protected $destination_exceptions = [
    'paragraph' => 'entity_reference_revisions:paragraph',
  ];

  /**
   * MigrationConfigDeriver constructor.
   * @param $base_plugin_id
   *   The base plugin ID for the plugin ID.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *  The EntityTypeManager.
   * @param \Drupal\core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\migrate\Plugin\MigrationPluginManager $migration_plugin_manager
   *  The migration plugin manager.
   * @param \Drupal\migrate_gathercontent\Plugin\GatherContentFieldPluginManager $gathercontent_field_plugin_manager
   *  The gathercontent field plugin manager.
   * @param \Drupal\migrate_gathercontent\DrupalGatherContentClient $gathercontent_client
   *  The GatherContentClient.
   */
  public function __construct($base_plugin_id,  EntityTypeManager $entityTypeManager, EntityFieldManagerInterface $entity_field_manager, MigrationPluginManager $migration_plugin_manager, GatherContentFieldPluginManager $gathercontent_field_plugin_manager,  DrupalGatherContentClient $gathercontent_client) {
    $this->basePluginId = $base_plugin_id;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entity_field_manager;
    $this->migrationPluginManager = $migration_plugin_manager;
    $this->gatherContentFieldPluginManager = $gathercontent_field_plugin_manager;
    $this->client = $gathercontent_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    // Translations don't make sense unless we have content_translation.
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.migration'),
      $container->get('plugin.manager.migrate.gathercontent_field'),
      $container->get('migrate_gathercontent.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    // Only load mappings that are enabled.
    $mapping_entities = $this->entityTypeManager->getStorage('gathercontent_mapping')->loadByProperties([
      'status' => TRUE,
    ]);

    if (!empty($mapping_entities)) {
      foreach ($mapping_entities as $entity) {

        $definition = $base_plugin_definition;

        // Migration dependencies.
        $dependencies = [];

        // Load field definitions.
        $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity->get('entity_type'), $entity->get('bundle'));

        // If no field mappings are set then don't define a migration.
        $field_mappings = $entity->getFieldMappings();
        if (!empty($field_mappings)) {
          unset($definition['deriver']);

          $definition['id'] = $entity->id();
          $definition['label'] = $entity->label();
          $definition['source']['project_id'] = $entity->get('project_id');
          $definition['source']['template'] = $entity->get('template');

          // Setting the bundle key.
          // This is necessary for supporting different entity bundles like
          // nodes, terms etc.
          $bundle_key = $this->entityTypeManager->getDefinition($entity->get('entity_type'))->getKey('bundle');
          $definition['process'][$bundle_key] = [
              'plugin' => 'default_value',
              'default_value' => $entity->get('bundle'),
          ];

          // Doing some tasks before setting up process plugins.
          foreach ($field_mappings as $source => $field_info) {
            // Creating compound sources.
            // Handling when a field is mapped to two or more sources.
            // We combine the values from those sources into one source and
            // then it goes through the normal process pipeline.
            if (!empty($field_info['field'])) {
              $field = $field_info['field'];
              if ($this->hasMultipleSources($field, $entity)) {
                $definition['process'] += $this->createCompoundSource($field, $entity);
              }
            }

            // Add required migration dependencies. Note this will be
            // reversed because the order of the migrations matters.
            if ($source_mapping = $this->loadMapping($source)) {
              $dependencies['required'][] = $source_mapping->getMigrationId();
            }
          }

          // Set migration dependencies.
          if (!empty($dependencies)) {
            $definition['migration_dependencies'] = array_reverse($dependencies);
          }

          // Set destination.
          // Check to see if there are any exceptions.
          if (!empty($this->destination_exceptions[$entity->get('entity_type')])) {
            $destination = $this->destination_exceptions[$entity->get('entity_type')];
          }
          // Otherwise use default destination plugin.
          else {
            $destination = 'entity:' . $entity->get('entity_type');
          }

          $definition['destination'] = [
            'plugin' => $destination,
            'default_bundle' => $entity->get('bundle'),
          ];

          $migration = $this->migrationPluginManager->createStubMigration($definition);

          // Begin process pipeline.
          foreach ($field_mappings as $source => $field_info) {
            if (!empty($field_info['field'])) {
              $field = $field_info['field'];

              // Checking for compound sources.
              // If there are any use that instead.
              if ($this->hasMultipleSources($field, $entity)) {
                $source = '@_compound_' . $field;
              }

              // Only add process plugin
              $process_plugin = $migration->getProcessPlugins();

              if (!empty($field_definitions[$field]) && empty($process_plugin[$field])) {
                $type = $field_definitions[$field]->getType();
                $plugin_id = $this->gatherContentFieldPluginManager->getPluginIdFromFieldType($type, [], $migration);
                if (!empty($plugin_id)) {
                  $plugin = $this->gatherContentFieldPluginManager->createInstance($plugin_id, [], $migration);
                  $plugin->defineValueProcessPipeline($migration, $field, $source, $entity);
                }

                // If no field type process exists them use default process.
                else {
                  // List fields do not work with deltas on multi value fields.
                  $process = [
                    'plugin' => 'get',
                    'source' => $source,
                  ];
                  $migration->mergeProcessOfProperty($field, $process);
                }

              }
            }
          }

          $this->derivatives[$entity->id()] = $migration->getPluginDefinition();

        }
      }
    }

    return $this->derivatives;
  }

  /**
   * This combines values from multiple sources into a single source.
   *
   * @param $field_name
   * @param $entity
   * @return array
   */
  private function createCompoundSource($field_name, $entity) {
    // TODO: refactor this to make it more concise.
    $process = [];
    $multi_sources = [];
    $field_mappings = $entity->getFieldMappings();
    if ($this->hasMultipleSources($field_name, $entity)) {
      foreach($field_mappings as $key => $field_info) {
        if ($field_info['field'] == $field_name) {
          $multi_sources[] = $key;
        }
      }
    }

    // Creating prepared sources.
    if (!empty($multi_sources)) {
      $prepared_sources = [];
      foreach ($multi_sources as  $source) {
        // If the source is a migration then look up those values first.
        if ($source_mapping = $this->loadMapping($source)) {
          $process['_prepare_' . $source][] = [
            'plugin' => 'migration_lookup',
            'migration' => $source_mapping->getMigrationId(),
            'source' => 'id',
          ];
        }
        // Otherwise use normal 'get' plugin.
        else {
          $process['_prepare_' . $source][] = [
            'plugin' => 'get',
            'source' => $source,
          ];
        }

        $prepared_sources[] = '@_prepare_' . $source;
      }

      // Creating the compound field, we refer to this field as the source.
      // e.g $source = '@_compound_' . $field_name.
      $process['_compound_' . $field_name][] = [
        'plugin' => 'get',
        'source' => $prepared_sources,
      ];

      // Only arrays from fields should be flattened.
      if (!$this->loadMapping($source)) {
        $process['_compound_' . $field_name][] = [
          'plugin' => 'flatten',
        ];
      }
    }

    return $process;
  }

  /**
   * Helper function for determining if a field has multiple sources defined.
   *
   * @param $field_name
   * @param $entity
   * @return bool
   */
  private function hasMultipleSources($field_name, $entity) {

    $field_mappings = $entity->getFieldMappings();
    foreach ($field_mappings as $id => $value) {
      $values[$id] = $value['field'];
    }
    $value_count = array_count_values($values);
    if (isset($value_count[$field_name])) {
      return ($value_count[$field_name] > 1);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Helper function to determine if mapping is a migration or field.
   *
   * @param string $source
   * @return bool
   */
  private function loadMapping($source) {
    $entity = $this->entityTypeManager->getStorage('gathercontent_mapping')->load($source);
    if (!empty($entity)) {
      return $entity;
    }

    return FALSE;
  }
}
