<?php

namespace Drupal\migrate_gathercontent\Plugin\migrate\field;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Plugin implementation of the 'boolean_checkbox' widget.
 *
 * @GatherContentField(
 *   id = "link",
 *   label = @Translation("Link"),
 *   field_types = {
 *     "link",
 *   }
 * )
 */
class LinkField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $source, $entity) {
    // TODO: Add support for link text.
    $process[] = [
      'plugin' => 'callback',
      'callable' => 'strip_tags',
      'source' => $source,
    ];
    $process[] = [
      'plugin' => 'callback',
      'callable' => 'trim',
    ];
    $migration->setProcessOfProperty($field_name . '/uri', $process);

  }

}
