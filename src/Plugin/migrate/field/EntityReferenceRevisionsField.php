<?php

namespace Drupal\migrate_gathercontent\Plugin\migrate\field;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Plugin implementation of the 'string' field.
 *
 * @GatherContentField(
 *   id = "entity_reference_revisions",
 *   label = @Translation("Entity reference revisions"),
 *   field_types = {
 *     "entity_reference_revisions",
 *   }
 * )
 */
class EntityReferenceRevisionsField extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs Entity Reference Field
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $source, $entity) {

    // If the source is a migration then use migration lookup.
    $mapping = $this->entityTypeManager->getStorage('gathercontent_mapping')->load($source);
    if (!empty($mapping)) {
      $process[] = [
        'plugin' => 'deepen',
        'source' => 'id',
        'keyname' => 'value',
      ];
      $process[] = [
        'plugin' => 'sub_process',
        'process' => [
          'temporary_id' => [
            'plugin' => 'migration_lookup',
            'migration' => $mapping->getMigrationId(),
            'source' => 'value',
          ],
          'target_id' => [
            'plugin' => 'extract',
            'source' => '@temporary_id',
            'index' => [0],
          ],
          'target_revision_id' => [
            'plugin' => 'extract',
            'source' => '@temporary_id',
            'index' => [1],
          ]
        ]
      ];

      $migration->setProcessOfProperty($field_name, $process);
    }

    // Note: This only executes for fields with multiple sources.
    else {
      $process[] = [
        'plugin' => 'deepen',
        'source' => $source,
        'keyname' => 'value',
      ];
      $process[] = [
        'plugin' => 'sub_process',
        'process' => [
          'target_id' => [
            'source' => 'value',
            'plugin' => 'extract',
            'index' => [0],
          ],
          'target_revision_id' => [
            'source' => 'value',
            'plugin' => 'extract',
            'index' => [1],
          ]
        ]
      ];

      $migration->setProcessOfProperty($field_name, $process);

    }
  }

}
