<?php

namespace Drupal\migrate_gathercontent\Plugin\migrate\field;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Plugin implementation of the 'string' field.
 *
 * @GatherContentField(
 *   id = "entity_reference",
 *   label = @Translation("Entity reference"),
 *   field_types = {
 *     "entity_reference",
 *   }
 * )
 */
class EntityReferenceField extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The selection plugin manager.
   *
   * @var \Drupal\core\Entity\EntityReferenceSelection\SelectionPluginManager
   */
  protected $selectionPluginManager;

  /**
   * Constructs Entity Reference Field
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selection_plugin_manager
   *   The selection plugin manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, SelectionPluginManagerInterface $selection_plugin_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->selectionPluginManager = $selection_plugin_manager;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_reference_selection'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $source, $entity) {

    // If source is a migration then use migration lookup.
    $mapping = $this->entityTypeManager->getStorage('gathercontent_mapping')->load($source);
    if (!empty($mapping)) {
      $process[] = [
        'plugin' => 'deepen',
        'source' => 'id',
        'keyname' => 'value',
      ];
      $process[] = [
        'plugin' => 'sub_process',
        'process' => [
          'target_id' => [
            'plugin' => 'migration_lookup',
            'migration' => $mapping->getMigrationId(),
            'source' => 'value',
          ],
        ]
      ];
      $migration->setProcessOfProperty($field_name, $process);

    }

    // Otherwise use entity lookup.
    else {
      $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity->get('entity_type'), $entity->get('bundle'));
      $lookupInformation = $this->getLookupInformation($field_definitions[$field_name]);

      $process[] = [
        'plugin' => 'deepen',
        'source' => $source,
        'keyname' => 'value',
      ];
      $process[] = [
        'plugin' => 'sub_process',
        'process' => [
          'target_id' => [
            'plugin' => 'gathercontent_entity_lookup',
            'source' => 'value',
            'destination_field' => $field_name,
            'value_key' => $lookupInformation['value_key'],
            'entity_type' => $lookupInformation['entity_type'],
            'bundle_key' => $lookupInformation['bundle_key'],
            'bundle' => $lookupInformation['bundle'],
          ]
        ]
      ];

      $migration->setProcessOfProperty($field_name, $process);
    }

  }

  /**
   * Helper function for fetching field information for entity_lookup.
   *
   * @param $field_config
   *   Field config information
   *
   * @return array
   *   Lookup information
   */
  private function getLookupInformation($field_config) {
    $handlerSettings = $field_config->getSetting('handler_settings');
    $bundles = array_filter((array) $handlerSettings['target_bundles']);
    if (count($bundles) == 1) {
      $lookupBundle = reset($bundles);
    }
    // This was added in 8.1.x is not supported in 8.0.x.
    elseif (!empty($handlerSettings['auto_create']) && !empty($handlerSettings['auto_create_bundle'])) {
      $lookupBundle = reset($handlerSettings['auto_create_bundle']);
    }
    else {
      $lookupBundle = array_values($bundles);
    }

    // Make an assumption that if the selection handler can target more
    // than one type of entity that we will use the first entity type.
    $lookupEntityType = reset($this->selectionPluginManager->createInstance($field_config->getSetting('handler'))->getPluginDefinition()['entity_types']);
    $lookupValueKey = $this->entityTypeManager->getDefinition($lookupEntityType)->getKey('label');
    $lookupBundleKey = $this->entityTypeManager->getDefinition($lookupEntityType)->getKey('bundle');

    // Note in some cases $lookupBundle will have an array of values.
    $info = [
      'value_key' => $lookupValueKey,
      'entity_type' => $lookupEntityType,
      'bundle_key' => $lookupBundleKey,
      'bundle' => $lookupBundle,
    ];

    return $info;
  }

}
