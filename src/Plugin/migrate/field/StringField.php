<?php

namespace Drupal\migrate_gathercontent\Plugin\migrate\field;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Plugin implementation of the 'string' field.
 *
 * @GatherContentField(
 *   id = "string",
 *   label = @Translation("String"),
 *   field_types = {
 *     "string",
 *     "string_long",
 *   }
 * )
 */
class StringField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $source, $entity) {
    // Note: Callable does not accept an array of callbacks.
    $process[] = [
      'plugin' => 'callback',
      'callable' => 'strip_tags',
      'source' => $source,
    ];
    $process[] = [
      'plugin' => 'callback',
      'callable' => 'trim',
    ];
    $migration->setProcessOfProperty($field_name, $process);

  }

}
