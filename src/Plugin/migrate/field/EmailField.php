<?php

namespace Drupal\migrate_gathercontent\Plugin\migrate\field;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Plugin implementation of the 'string' field.
 *
 * @GatherContentField(
 *   id = "email",
 *   label = @Translation("Email"),
 *   field_types = {
 *     "email",
 *   }
 * )
 */
class EmailField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $source, $entity) {
    // Note: Callable does not accept an array of callbacks.
    $process[] = [
      'plugin' => 'callback',
      'callable' => 'strip_tags',
      'source' => $source,
    ];
    $process[] = [
      'plugin' => 'callback',
      'callable' => 'trim',
    ];

    $migration->setProcessOfProperty($field_name, $process);

  }

}
